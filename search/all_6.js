var searchData=
[
  ['get_0',['get',['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares::Queue']]],
  ['get_5faccel_1',['get_accel',['../classBNO055_1_1BNO055.html#a1676a6454e0615f8a84ff5411811daa0',1,'BNO055::BNO055']]],
  ['get_5fadc_2',['get_adc',['../classpanel_1_1Panel.html#ad1002338f60d64237de98ec761e7a95b',1,'panel::Panel']]],
  ['get_5fcalibration_5fcoeff_3',['get_calibration_coeff',['../classBNO055_1_1BNO055.html#af06124d01a721c06513c8e4af1d156d2',1,'BNO055::BNO055']]],
  ['get_5fcalibration_5fstatus_4',['get_calibration_status',['../classBNO055_1_1BNO055.html#aaa9dfe166380cf324009a27060cf00c7',1,'BNO055::BNO055']]],
  ['get_5fcoord_5',['get_coord',['../classpanel_1_1Panel.html#a708c39b1dab355ace095da1c1f118fc3',1,'panel::Panel']]],
  ['get_5feuler_6',['get_euler',['../classBNO055_1_1BNO055.html#a1873696ee7301f1188f5cdbbeb1d1900',1,'BNO055::BNO055']]],
  ['get_5fomega_7',['get_omega',['../classBNO055_1_1BNO055.html#a5cc089e83e452a99b718645047c07f43',1,'BNO055::BNO055']]]
];
