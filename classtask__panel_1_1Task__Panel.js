var classtask__panel_1_1Task__Panel =
[
    [ "__init__", "classtask__panel_1_1Task__Panel.html#a4f64dde5589fdc6f077aa0ae67a32396", null ],
    [ "run", "classtask__panel_1_1Task__Panel.html#a30306af729d3bd06cd6b224d08b85115", null ],
    [ "alpha", "classtask__panel_1_1Task__Panel.html#a38fb8c282fca27f5f5169dd79be53940", null ],
    [ "beta", "classtask__panel_1_1Task__Panel.html#a3b2c79e70f01d91b5522740b267eddcb", null ],
    [ "dx", "classtask__panel_1_1Task__Panel.html#acf88513459bd540f0fd07664217840bc", null ],
    [ "dy", "classtask__panel_1_1Task__Panel.html#adce3fca913638cc976afe61aad027d91", null ],
    [ "next_ticks", "classtask__panel_1_1Task__Panel.html#a4f96604692af6d4890cb0daab41b2422", null ],
    [ "panel", "classtask__panel_1_1Task__Panel.html#ae9e830b4355181167b9cd685f333e396", null ],
    [ "posfilter", "classtask__panel_1_1Task__Panel.html#a299568de2855445656f1194f0cc7df5f", null ],
    [ "runs", "classtask__panel_1_1Task__Panel.html#a54e1c585a59df5c20c1b6ad537ceb93b", null ],
    [ "sampletime", "classtask__panel_1_1Task__Panel.html#a85c2391da3816b8f732c955f44a566a1", null ],
    [ "start_ticks", "classtask__panel_1_1Task__Panel.html#a3a7b356b125b0b611e4995773275dd83", null ],
    [ "touchbool", "classtask__panel_1_1Task__Panel.html#a4a89f2dc6a93e7913d4ed7b8797dc82b", null ],
    [ "x", "classtask__panel_1_1Task__Panel.html#ab9cbc9050a9fad67426fcc652d55c30d", null ],
    [ "y", "classtask__panel_1_1Task__Panel.html#a655a66c21d640a66303a6205bd241117", null ]
];